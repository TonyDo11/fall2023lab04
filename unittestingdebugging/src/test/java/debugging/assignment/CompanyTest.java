package debugging.assignment;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test Company class
 */
public class CompanyTest
{
    /**
     * Checks for different employees
     */
    @Test
    public void testDifferentEmployees()
    {
        Employee[] employees = new Employee[] { new Employee("Dan", 12345), new Employee("Gabriela", 123), new Employee("Andrew", 12)};

        Company c1 = new Company(employees);
        System.out.println(employees[0].getEmployeeId());
        // change it so that it's a different company
        employees[0].setEmployeeId(12);
        System.out.println(employees[0].getEmployeeId());
        Company c2 = new Company(employees);
        assertNotEquals(c1, c2);

    }
}
